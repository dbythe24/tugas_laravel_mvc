<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
route::get('/register', function () {
    return view('register');
});
route::get('/welcome', function () {
    return view('welcome');
});

// route parameter
// route::get('/test/{nama}', function ($nama){
//     return "halo $nama";
// });

//controller
route::get('/', 'HomeController@home');
route::get('/register', 'AuthController@register');
//route::get('/welcome', 'AuthController@welcome');
route::post('/welcome', 'AuthController@welcome');
